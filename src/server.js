require ('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./routes/main');
const secureRoutes = require('./routes/secure');
const Database = require('./db.js');
//Database connection
const db = new Database();
db.createTable();

const app = express();

//update express settings
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//main routes
app.use('/', routes);
app.use('/', secureRoutes);

app.use((err, req, res, next) => {
	res.status(err.status || 500);
	res.json({error: err});
});

app.listen(3000, () => {
	console.log('server listening on port 3000');
});