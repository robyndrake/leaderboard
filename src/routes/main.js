const express = require('express');

const router = express.Router();

router.get('/status', (req, res, next) => {
	res.status(200);
	res.json({'status': 'ok', "message": "status"});
});

router.post('/signup', (req, res, next) => {
	res.status(200);
	res.json({'status': 'ok', 'message': 'signup'});
});

router.post('/login', (req, res, next) => {
	res.status(200);
	res.json({'status': 'ok', 'message': 'login'});
});

router.post('/logout', (req, res, next) => {
	res.status(200);
	res.json({'status': 'ok', 'message': 'logout'});
});

router.post('/token', (req, res, next) => {
	res.status(200);
	res.json({'status': 'ok', 'message': 'token'});
});

module.exports = router;