const express = require('express');

const router = express.Router();

router.post('/submit-score', (req, res, next) => {
	res.status(200);
	res.json({'status': 'ok', 'message': 'submit-score'});
});

router.get('/scores', (req, res, next) => {
	res.status(200);
	res.json({'status': 'ok', 'message': 'scores'});
});

module.exports = router;