const path = require('path');
const bcrypt = require('bcryptjs');
const Str = require('@supercharge/strings');

class Database {
	constructor() {
		this.dbpath = path.resolve(__dirname, 'database/users.db');
		this.knex = require('knex')({
			client: 'sqlite3',
			connection: {
				filename: this.dbpath
			},
			useNullAsDefault: true
		});
		this.table = 'users';
	}

	createTable() {
		this.knex.schema
		.hasTable(this.table)
		.then((exists) => {
			if(!exists) {
				//if no user table exists
				return knex.schema.createTable('users', (table) => {
					table.increment('id').primary();
					table.string('email');
					table.string('seed');
					table.string('password');
				})
				.then(() => {
					console.log("Table 'users' created");
				})
				.catch((error) => {
					console.error(`There was an error: ${error.message}`);
				})
			}
		})
		.then(() => {
			console.log('done');
		})
		.catch((error) => {
			console.error("There was an error setting up the database");
		});
	}

	insert(email, password) {
		let seed = Str.random(10);
		let hash = bcrypt.hashSync(password, seed);
		this.knex(this.table).insert(email, seed, hash);
	}


};


module.exports = Database